#include "Quadrangle.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;

std::string getPointInfo(const Point& p)
{
	return "[X = " + std::to_string(p.getX()) + ", Y = " + std::to_string(p.getY()) + "]";
}

std::string getQuadranglePoints(const std::vector<Point>& points)
{
	return "{" + getPointInfo(points[0]) + ", " + getPointInfo(points[1]) +
		",\n\t" + getPointInfo(points[2]) + ", " + getPointInfo(points[3]) + "}";
}

std::string getQuadrangleInfo(const Quadrangle& q)
{
	return "Name = " + q.getName() + ", type = " + q.getType() + "\n" +
		"Perimeter = " + std::to_string(q.getPerimeter()) +
		", Area = " + std::to_string(q.getArea()) + "\n"
		"Points:\n\t" + getQuadranglePoints(q.getPoints());
}

bool testBonus()
{
	bool result = false;

	try
	{
		// Tests Ex5 part 1 - Rectangle

		cout <<
			"*******************\n" <<
			"Test 6 - Quadrangle \n" <<
			"*******************\n" << endl;

		cout <<
			"Initializing new Quadrangle object q1 with P1 = (-5,7) , P2 = (2,6), P3 = (-4,0) , P4 = Point(5,-3)... \n" << endl;
		Quadrangle q1(Point(-5, 7), Point(2, 6), Point(-4, 0), Point(5, -3), "quadrangle", "q1");
		std::string expected = "Name = q1, type = quadrangle\n";
		expected += "Perimeter = 39.185318, Area = 68.025683\n";
		expected += "Points:\n";
		expected += "\t{[X = -5.000000, Y = 7.000000], [X = 2.000000, Y = 6.000000],\n";
		expected += "\t[X = -4.000000, Y = 0.000000], [X = 5.000000, Y = -3.000000]}";

		std::string got = getQuadrangleInfo(q1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			std::cout << " \n" << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5601");
			std::cout << " \n" << endl;
			return 5601;
		}

		cout <<
			"Initializing new Quadrangle object q2 with P1 = (56,-100) , P2 = (206, -100), P3 = (206, 150) , P4 = Point(56, 150)... \n" << endl;
		Quadrangle q2(Point(56, -100), Point(206, -100), Point(206, 150), Point(56, 150), "quadrangle", "q2");

		expected = "Name = q2, type = quadrangle\n";
		expected += "Perimeter = 800.000000, Area = 37500.000000\n";
		expected += "Points:\n";
		expected += "\t{[X = 56.000000, Y = -100.000000], [X = 206.000000, Y = -100.000000],\n";
		expected += "\t[X = 206.000000, Y = 150.000000], [X = 56.000000, Y = 150.000000]}";

		got = getQuadrangleInfo(q2);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			std::cout << " \n" << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5601");
			std::cout << " \n" << endl;
			return 5601;
		}



	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	return 0;

}


int main()
{
	std::cout <<
		"###########################\n" <<
		"Exercise 5 - Shapes\n" <<
		"Part 1 Bonus - Quadrangle\n" <<
		"###########################\n" << std::endl;

	int testResult = testBonus();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex5 Part 1 Bonus Tests Passed ******\033[0m\n \n" : "\033[1;31mEx5 Part 1 Bonus Tests Failed\033[0m\n \n") << endl;

	return testResult;
}