#include "Arrow.h"
#include "Circle.h"
#include "Triangle.h"
#include "Rectangle.h"
#include <vector>
#include <iostream>
#include <string>

using std::cout;
using std::endl;

std::string getPointInfo(const Point& p)
{
	return "[X = " + std::to_string(p.getX()) + ", Y = " + std::to_string(p.getY()) + "]";
}


std::string getShapeInfo(const Shape& s)
{
	return "Name = " + s.getName() + ", type = " + s.getType() + "\n" +
		"Perimeter = " + std::to_string(s.getPerimeter()) +
		", Area = " + std::to_string(s.getArea());
}

bool testMemory()
{
	bool result = false;

	try
	{
		// Tests Ex5 part 1 - Memory Test

		cout <<
			"************\n" <<
			"Memory Test \n" <<
			"************\n" << endl;

		cout <<
		"\nInitializing Shape objects  ... \n" << endl;

		std::vector<Shape*> shapes;
		shapes.push_back(new Arrow (Point(0, 0), Point(13, 14), "arrow", "a1"));
		shapes.push_back(new Circle(Point(-100, 34), 25, "circle", "c1"));
		shapes.push_back(new Triangle(Point(0, 0), Point(3, 0), Point(0, 4), "triangle", "t1"));
		shapes.push_back(new myShapes::Rectangle(Point(56, -100), 150, -250, "rectangle", "r1"));
		shapes.push_back(new Circle(Point(0, 0), 10, "circle", "c2"));
		shapes.push_back(new Arrow(Point(-50, -50), Point(10, 100), "arrow", "a2"));
		shapes.push_back(new Triangle(Point(0, 0), Point(-12, 0), Point(0, -5), "triangle", "t2"));
		shapes.push_back(new myShapes::Rectangle(Point(0, 0), 200, 120, "rectangle", "r2"));


		cout <<
			"\nPrinting Shape objects information ... \n" << endl;

		for (unsigned int i = 0; i < shapes.size(); i++)
		{
			std::cout << getShapeInfo(*shapes[i]) << std::endl;
		}

		cout <<
			"\nDeleting Shape objects ... \n" << endl;

		for (unsigned int i = 0; i < shapes.size(); i++)
		{
			delete shapes[i];
		}
		shapes.clear();
	}
	catch (...)
	{
		std::cerr << "Test crashed" << endl;
		std::cout << "FAILED: The program crashed, check the following things:\n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return false;
	}

	cout << "\n########## Memory - TEST Complete Without Failures!!! ##########\n\n";

	return true;

}


int main()
{
	std::cout <<
		"###########################\n" <<
		"Exercise 5 - Shapes\n" <<
		"Part 1 Test - Memory\n" <<
		"###########################\n" << std::endl;

	bool testResult = testMemory();

	if (testResult)
	{
		std::cout << "\n########## Ex5 Memory Test Passed!!! ##########" << "\n\n";
	}
	else
	{
		std::cout << "\n########## TEST Failed :( ##########\n";
	}
	return testResult ? 0 : 1;
}