#include "Point.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;

std::string getPointInfo(const Point& p)
{
	return "[X = " + std::to_string(p.getX()) + ", Y = " + std::to_string(p.getY()) + "]";
}

int test1Point()
{

	try
	{
		// Tests Ex5 part 1 - Point

		cout <<
			"****************\n" <<
			"Test 1 - Point \n" <<
			"****************\n" << endl;

		cout <<
			"Initializing new Point object p1 ... \n" << endl;
		Point p1;

		std::string expected = "[X = 0.000000, Y = 0.000000]";
		std::string got = getPointInfo(p1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5101");
			std::cout << " \n" << endl;
			return 5101;
		}

		cout <<
			"\nInitializing new Point object p2 with X=10 , Y=10 ... \n" << endl;

		Point p2(10, 10);
		expected = "[X = 10.000000, Y = 10.000000]";
		got = getPointInfo(p2);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5102");
			std::cout << " \n" << endl;
			return 5102;
		}

		cout <<
			"\nCalling p1.distance(p2) ... \n" << endl;

		expected = "14.142136";
		got = std::to_string(p1.distance(p2));
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5103");
			std::cout << " \n" << endl;
			return 5103;
		}

		cout <<
			"\nInitializing new Point object p3 with X=-15 , Y=12 ... \n" << endl;

		Point p3(-15, 12);
		expected = "[X = -15.000000, Y = 12.000000]";
		got = getPointInfo(p3);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5102");
			std::cout << " \n" << endl;
			return 5102;
		}

		cout <<
			"\nUsing operator + p2 + p3 ... \n" << endl;

		const Point presult = p2 + p3;

		expected = "[X = -5.000000, Y = 22.000000]";
		got = getPointInfo(presult);;
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5104");
			std::cout << " \n" << endl;
			return 5104;
		}

		cout <<
			"\nCalling p2.distance(p3) ... \n" << endl;

		expected = "25.079872";
		got = std::to_string(p2.distance(p3));
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5103");
			std::cout << " \n" << endl;
			return 5103;
		}

		cout <<
			"\nInitializing new Point object p4 with X=-6 , Y=-8 ... \n" << endl;

		Point p4(-6, -8);
		expected = "[X = -6.000000, Y = -8.000000]";
		got = getPointInfo(p4);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5102");
			std::cout << " \n" << endl;
			return 5102;
		}
		
		cout <<
			"\nCalling p4.distance(p1) ... \n" << endl;

		expected = "10.000000";
		got = std::to_string(p4.distance(p1));
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5103");
			std::cout << " \n" << endl;
			return 5103;
		}

		cout <<
			"\nCalling p4.distance(p2) ... \n" << endl;

		expected = "24.083189";
		got = std::to_string(p4.distance(p2));
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5103");
			std::cout << " \n" << endl;
			return 5103;
		}

		cout <<
			"\nUsing operator += p3 += p1 ... \n" << endl;

		p3 += p1;

		expected = "[X = -15.000000, Y = 12.000000]";
		got = getPointInfo(p3);;
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5105");
			std::cout << " \n" << endl;
			return 5105;
		}

		cout <<
			"\nUsing operator += p3 += p3 ... \n" << endl;

		p3 += p3;

		expected = "[X = -30.000000, Y = 24.000000]";
		got = getPointInfo(p3);;
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5105");
			std::cout << " \n" << endl;
			return 5105;
		}

	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	return 0;

}


int main()
{
	std::cout <<
		"########################\n" <<
		"Exercise 5 - Shapes\n" <<
		"Part 1 - Point\n" <<
		"########################\n" << std::endl;

	int testResult = test1Point();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex5 Part 1A Tests Passed ******\033[0m\n \n" : "\033[1;31mEx5 Part 1A Tests Failed\033[0m\n \n") << endl;

	return testResult;
}