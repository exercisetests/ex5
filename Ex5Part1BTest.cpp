#include "Arrow.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;


std::string getPointInfo(const Point& p)
{
	return "[X = " + std::to_string(p.getX()) + ", Y = " + std::to_string(p.getY()) + "]";
}

std::string getArrowInfo(const Arrow& a)
{
	return "Name = " + a.getName() + ", type = " + a.getType() + "\n" +
		"Perimeter = " + std::to_string(a.getPerimeter()) +
		", Area = " + std::to_string(a.getArea()) + "\n" +
		"Source = " + getPointInfo(a.getSource()) +
		" , Destination = " + getPointInfo(a.getDestination());
}

int test2Arrow()
{

	try
	{
		// Tests Ex5 part 1 - Arrow

		cout <<
			"****************\n" <<
			"Test 2 - Arrow \n" <<
			"****************\n" << endl;

		cout <<
			"Initializing new Arrow object a1 with P1 = (0,0) , P2 = (13,14) ... \n" << endl;
		Arrow a1(Point(0, 0), Point(13, 14), "arrow", "a1");

		std::string expected = "Name = a1, type = arrow\n";
		expected += "Perimeter = 19.104973, Area = 0.000000\n";
		expected += "Source = [X = 0.000000, Y = 0.000000] , Destination = [X = 13.000000, Y = 14.000000]";
		std::string got = getArrowInfo(a1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5201");
			std::cout << " \n" << endl;
			return 5201;
		}

		cout <<
			"\nMoving Arrow a1.move(10,10) ... \n" << endl;

		a1.move(Point(10, 10));
		expected = "Name = a1, type = arrow\n";
		expected += "Perimeter = 19.104973, Area = 0.000000\n";
		expected += "Source = [X = 10.000000, Y = 10.000000] , Destination = [X = 23.000000, Y = 24.000000]";
		got = getArrowInfo(a1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5202");
			std::cout << " \n" << endl;
			return 5202;
		}

		cout <<
			"\nMoving Arrow a1.move(-15,-120) ... \n" << endl;

		a1.move(Point(-15, -120));
		expected = "Name = a1, type = arrow\n";
		expected += "Perimeter = 19.104973, Area = 0.000000\n";
		expected += "Source = [X = -5.000000, Y = -110.000000] , Destination = [X = 8.000000, Y = -96.000000]";
		got = getArrowInfo(a1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5202");
			std::cout << " \n" << endl;
			return 5202;
		}

	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	return 0;
}


int main()
{
	std::cout <<
		"########################\n" <<
		"Exercise 5 - Shapes\n" <<
		"Part 1 - Arrow\n" <<
		"########################\n" << std::endl;

	int testResult = test2Arrow();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex5 Part 1B Tests Passed ******\033[0m\n \n" : "\033[1;31mEx5 Part 1B Tests Failed\033[0m\n \n") << endl;

	return testResult;
}