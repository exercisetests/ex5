#include "Circle.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;

std::string getPointInfo(const Point& p)
{
	return "[X = " + std::to_string(p.getX()) + ", Y = " + std::to_string(p.getY()) + "]";
}

std::string getCircleInfo(const Circle& c)
{
	return "Name = " + c.getName() + ", type = " + c.getType() + "\n" +
		"Center = " + getPointInfo(c.getCenter()) +
		", Radius = " + std::to_string(c.getRadius()) + "\n" +
		"Perimeter = " + std::to_string(c.getPerimeter()) +
		", Area = " + std::to_string(c.getArea());
}

int test3Circle()
{

	try
	{
		// Tests Ex5 part 1 - Circle

		cout <<
			"****************\n" <<
			"Test 3 - Circle \n" <<
			"****************\n" << endl;

		cout <<
			"Initializing new Circle object c1 with Radius = 7 , Center = (5,7) ... \n" << endl;
		Circle c1(Point(5, 7), 7, "circle", "c1");

		std::string expected = "Name = c1, type = circle\n";
		expected += "Center = [X = 5.000000, Y = 7.000000], Radius = 7.000000\n";
		expected += "Perimeter = 43.960000, Area = 153.860000";
		std::string got = getCircleInfo(c1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5301");
			std::cout << " \n" << endl;
			return 5301;
		}

		cout <<
			"\nMoving Circle c1.move(10,10) ... \n" << endl;

		c1.move(Point(10, 10));
		expected = "Name = c1, type = circle\n";
		expected += "Center = [X = 15.000000, Y = 17.000000], Radius = 7.000000\n";
		expected += "Perimeter = 43.960000, Area = 153.860000";
		got = getCircleInfo(c1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5302");
			std::cout << " \n" << endl;
			return 5302;
		}

		cout <<
			"\nMoving Circle c1.move(-20,-14) ... \n" << endl;

		c1.move(Point(-20, -14));
		expected = "Name = c1, type = circle\n";
		expected += "Center = [X = -5.000000, Y = 3.000000], Radius = 7.000000\n";
		expected += "Perimeter = 43.960000, Area = 153.860000";
		got = getCircleInfo(c1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5302");
			std::cout << " \n" << endl;
			return 5302;
		}

		cout <<
			"\nInitializing new Circle object c1 with Radius = 25 , Center = (-100,34) ... \n" << endl;
		Circle c2(Point(-100, 34), 25, "circle", "c2");

		expected = "Name = c2, type = circle\n";
		expected += "Center = [X = -100.000000, Y = 34.000000], Radius = 25.000000\n";
		expected += "Perimeter = 157.000000, Area = 1962.500000";
		got = getCircleInfo(c2);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5301");
			std::cout << " \n" << endl;
			return 5301;
		}

		cout <<
			"\nMoving Circle c1.move(101,-34) ... \n" << endl;

		c2.move(Point(101, -34));
		expected = "Name = c2, type = circle\n";
		expected += "Center = [X = 1.000000, Y = 0.000000], Radius = 25.000000\n";
		expected += "Perimeter = 157.000000, Area = 1962.500000";
		got = getCircleInfo(c2);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5302");
			std::cout << " \n" << endl;
			return 5302;
		}

		cout <<
			"\nMoving Circle c1.move(150,200) ... \n" << endl;

		c2.move(Point(150, 200));
		expected = "Name = c2, type = circle\n";
		expected += "Center = [X = 151.000000, Y = 200.000000], Radius = 25.000000\n";
		expected += "Perimeter = 157.000000, Area = 1962.500000";
		got = getCircleInfo(c2);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5302");
			std::cout << " \n" << endl;
			return 5302;
		}

	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	return 0;

}


int main()
{
	std::cout <<
		"########################\n" <<
		"Exercise 5 - Shapes\n" <<
		"Part 1 - Circle\n" <<
		"########################\n" << std::endl;

	int testResult = test3Circle();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex5 Part 1C Tests Passed ******\033[0m\n \n" : "\033[1;31mEx5 Part 1C Tests Failed\033[0m\n \n") << endl;

	return testResult;
}