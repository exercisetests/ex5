#include "Triangle.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;

std::string getPointInfo(const Point& p)
{
	return "[X = " + std::to_string(p.getX()) + ", Y = " + std::to_string(p.getY()) + "]";
}

std::string getTrianglePoints(const std::vector<Point>& points)
{
	return "{" + getPointInfo(points[0]) + ", " + getPointInfo(points[1]) + ", " + getPointInfo(points[2]) + "}";
}

std::string getTriangleInfo(const Triangle& t)
{
	return "Name = " + t.getName() + ", type = " + t.getType() + "\n" +
		"Perimeter = " + std::to_string(t.getPerimeter()) +
		", Area = " + std::to_string(t.getArea()) + "\n"
		"Points: " + getTrianglePoints(t.getPoints());
}

int test4Triangle()
{

	try
	{
		// Tests Ex5 part 1 - Triangle

		cout <<
			"*******************\n" <<
			"Test 4 - Triangle \n" <<
			"*******************\n" << endl;

		cout <<
			"Initializing new Triangle object t1 with P1 = (12,-11) , P2 = (33,44), P3 = (-18,91) ... \n" << endl;
		Triangle t1(Point(12, -11), Point(33, 44), Point(-18, 91), "triangle", "t1");

		std::string expected = "Name = t1, type = triangle\n";
		expected += "Perimeter = 234.547179, Area = 1896.000000\n";
		expected += "Points: {[X = 12.000000, Y = -11.000000], [X = 33.000000, Y = 44.000000], [X = -18.000000, Y = 91.000000]}";
		std::string got = getTriangleInfo(t1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5401");
			std::cout << " \n" << endl;
			return 5401;
		}

		cout <<
			"\nMoving Triangle t1.move(100,100) ... \n" << endl;

		t1.move(Point(100, 100));
		expected = "Name = t1, type = triangle\n";
		expected += "Perimeter = 234.547179, Area = 1896.000000\n";
		expected += "Points: {[X = 112.000000, Y = 89.000000], [X = 133.000000, Y = 144.000000], [X = 82.000000, Y = 191.000000]}";
		got = getTriangleInfo(t1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5402");
			std::cout << " \n" << endl;
			return 5402;
		}

		cout <<
			"\nMoving Triangle a1.move(-50,-220) ... \n" << endl;

		t1.move(Point(-50, -220));
		expected = "Name = t1, type = triangle\n";
		expected += "Perimeter = 234.547179, Area = 1896.000000\n";
		expected += "Points: {[X = 62.000000, Y = -131.000000], [X = 83.000000, Y = -76.000000], [X = 32.000000, Y = -29.000000]}";
		got = getTriangleInfo(t1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5402");
			std::cout << " \n" << endl;
			return 5402;
		}

		// #########################
		// #  pythagorean triplet  #
		// #     < 3 , 4 , 5 >     #
		// #########################

		cout <<
			"\nInitializing new Triangle object t2 with P1 = (0,0) , P2 = (3,0), P3 = (0,4) ... \n" << endl;
		Triangle t2(Point(0, 0), Point(3, 0), Point(0, 4), "triangle", "t2");

		expected = "Name = t2, type = triangle\n";
		expected += "Perimeter = 12.000000, Area = 6.000000\n";
		expected += "Points: {[X = 0.000000, Y = 0.000000], [X = 3.000000, Y = 0.000000], [X = 0.000000, Y = 4.000000]}";
		got = getTriangleInfo(t2);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5401");
			std::cout << " \n" << endl;
			return 5401;
		}

		// #########################
		// #  pythagorean triplet  #
		// #     < 5 , 12 , 13 >     #
		// #########################

		cout <<
			"\nInitializing new Triangle object t3 with P1 = (0,0) , P2 = (-12,0), P3 = (0,-5) ... \n" << endl;
		Triangle t3(Point(0, 0), Point(-12, 0), Point(0, -5), "triangle", "t3");

		expected = "Name = t3, type = triangle\n";
		expected += "Perimeter = 30.000000, Area = 30.000000\n";
		expected += "Points: {[X = 0.000000, Y = 0.000000], [X = -12.000000, Y = 0.000000], [X = 0.000000, Y = -5.000000]}";
		got = getTriangleInfo(t3);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5401");
			std::cout << " \n" << endl;
			return 5401;
		}

	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	return 0;

}


int main()
{
	std::cout <<
		"########################\n" <<
		"Exercise 5 - Shapes\n" <<
		"Part 1 - Triangle\n" <<
		"########################\n" << std::endl;

	int testResult = test4Triangle();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex5 Part 1D Tests Passed ******\033[0m\n \n" : "\033[1;31mEx5 Part 1D Tests Failed\033[0m\n \n") << endl;

	return testResult;
}