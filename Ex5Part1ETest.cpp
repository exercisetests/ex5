#include "Rectangle.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;

std::string getPointInfo(const Point& p)
{
	return "[X = " + std::to_string(p.getX()) + ", Y = " + std::to_string(p.getY()) + "]";
}

std::string getRectanglePoints(const std::vector<Point>& points)
{
	return "{" + getPointInfo(points[0]) + ", " + getPointInfo(points[1]) +
		",\n\t" + getPointInfo(points[2]) + ", " + getPointInfo(points[3]) + "}";
}

std::string getRectangleInfo(const myShapes::Rectangle& t)
{
	return "Name = " + t.getName() + ", type = " + t.getType() + "\n" +
		"Perimeter = " + std::to_string(t.getPerimeter()) +
		", Area = " + std::to_string(t.getArea()) + "\n"
		"Points:\n\t" + getRectanglePoints(t.getPoints());
}

int test5Rectangle()
{

	try
	{
		// Tests Ex5 part 1 - Rectangle

		cout <<
			"*******************\n" <<
			"Test 5 - Rectangle \n" <<
			"*******************\n" << endl;

		cout <<
			"Initializing new Rectangle object r1 with P1 = (0,0) , Length = 134 , Width = 185 ... \n" << endl;
		myShapes::Rectangle r1(Point(0, 0), 134, 185, "rectangle", "r1");

		std::string expected = "Name = r1, type = rectangle\n";
		expected += "Perimeter = 638.000000, Area = 24790.000000\n";
		expected += "Points:\n";
		expected += "\t{[X = 0.000000, Y = 0.000000], [X = 134.000000, Y = 0.000000],\n";
		expected += "\t[X = 134.000000, Y = 185.000000], [X = 0.000000, Y = 185.000000]}";

		std::string got = getRectangleInfo(r1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5501");
			std::cout << " \n" << endl;
			return 5501;
		}

		cout <<
			"\nMoving Rectangle r1.move(100,100) ... \n" << endl;

		r1.move(Point(100, 100));
		expected = "Name = r1, type = rectangle\n";
		expected += "Perimeter = 638.000000, Area = 24790.000000\n";
		expected += "Points:\n";
		expected += "\t{[X = 100.000000, Y = 100.000000], [X = 234.000000, Y = 100.000000],\n";
		expected += "\t[X = 234.000000, Y = 285.000000], [X = 100.000000, Y = 285.000000]}";
		got = getRectangleInfo(r1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5502");
			std::cout << " \n" << endl;
			return 5502;
		}

		cout <<
			"\nMoving Rectangle a1.move(-350,-320) ... \n" << endl;

		r1.move(Point(-350, -320));
		expected = "Name = r1, type = rectangle\n";
		expected += "Perimeter = 638.000000, Area = 24790.000000\n";
		expected += "Points:\n";
		expected += "\t{[X = -250.000000, Y = -220.000000], [X = -116.000000, Y = -220.000000],\n";
		expected += "\t[X = -116.000000, Y = -35.000000], [X = -250.000000, Y = -35.000000]}";
		got = getRectangleInfo(r1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5502");
			std::cout << " \n" << endl;
			return 5502;
		}

		cout <<
			"Initializing new Rectangle object r2 with P1 = (56,-100) , Length = 150 , Width = 250 ... \n" << endl;
		myShapes::Rectangle r2(Point(56, -100), 150, 250, "rectangle", "r2");

		expected = "Name = r2, type = rectangle\n";
		expected += "Perimeter = 800.000000, Area = 37500.000000\n";
		expected += "Points:\n";
		expected += "\t{[X = 56.000000, Y = -100.000000], [X = 206.000000, Y = -100.000000],\n";
		expected += "\t[X = 206.000000, Y = 150.000000], [X = 56.000000, Y = 150.000000]}";

		got = getRectangleInfo(r2);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 5501");
			std::cout << " \n" << endl;
			return 5501;
		}

	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	return 0;


}


int main()
{
	std::cout <<
		"########################\n" <<
		"Exercise 5 - Shapes\n" <<
		"Part 1 - Rectangle\n" <<
		"########################\n" << std::endl;

	int testResult = test5Rectangle();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex5 Part 1E Tests Passed ******\033[0m\n \n" : "\033[1;31mEx5 Part 1E Tests Failed\033[0m\n \n") << endl;

	return testResult;
}
